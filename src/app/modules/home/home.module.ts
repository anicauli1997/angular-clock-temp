import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {RouterModule} from "@angular/router";
import {homeRouters} from "./home.routing";
import {TempClockComponent} from "../../shared/components/temp-clock/temp-clock.component";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    HomeComponent,
    TempClockComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(homeRouters),
    ReactiveFormsModule
  ]
})
export class HomeModule { }
