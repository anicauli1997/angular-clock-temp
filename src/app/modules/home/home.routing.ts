import {Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";

export const homeRouters: Routes = [
  {
    path: '',
    component: HomeComponent,
  }
]
