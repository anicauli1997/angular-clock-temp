import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {HomeService} from "../../../shared/services/home/home.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  public tempFormForm: FormGroup;
  public minTemp: number = 0;
  public maxTemp: number = 100;
  public targetTemp: number = 50;
  constructor(private homeService: HomeService) {
    this.tempFormForm = homeService.getTempFormGroup(this.minTemp, this.maxTemp, this.targetTemp);
  }

  submitTempForm(): void {
    if (!this.tempFormForm.valid) return;
    const min = this.tempFormForm.get('min')?.value;
    const max = this.tempFormForm.get('max')?.value;
    const target = this.tempFormForm.get('target')?.value;
    if (min >= max) {
      this.tempFormForm.get('max')?.setErrors({ lessEqualThanMin: true });
    }
    if (!(target >= min && target <= max)) {
      this.tempFormForm.get('target')?.setErrors({ outOfRange: true });
    }
    if (!this.tempFormForm.valid) return;
    this.minTemp = min;
    this.maxTemp = max;
    this.targetTemp = target;
  }
}
