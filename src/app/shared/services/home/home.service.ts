import { Injectable } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor() { }

  public getTempFormGroup(min: number = 0, max: number = 100, target: number = 50): FormGroup
  {
    return new FormGroup({
      min: new FormControl(min, [Validators.required]),
      max: new FormControl(max, [Validators.required]),
      target: new FormControl(target, [Validators.required]),
    });
  }
}
