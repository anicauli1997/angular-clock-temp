import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-temp-clock',
  templateUrl: './temp-clock.component.html',
  styleUrls: ['./temp-clock.component.css']
})
export class TempClockComponent {
  @Input() min: number = 0;
  @Input() max: number = 100;
  @Input() target: number = 50;

  constructor() { }

  get targetRotateStyle(): string {
    const totalDegrees = 270;
    const calculatedMax = this.max - this.min;
    const calculatedTarget = this.target - this.min;
    const calculatedDegrees = (!calculatedMax ? 0 : calculatedTarget/calculatedMax) * totalDegrees - 45;
    return `rotate(${calculatedDegrees}deg)`
  }
}
