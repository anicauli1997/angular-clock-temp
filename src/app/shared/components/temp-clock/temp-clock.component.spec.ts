import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempClockComponent } from './temp-clock.component';

describe('TempClockComponent', () => {
  let component: TempClockComponent;
  let fixture: ComponentFixture<TempClockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TempClockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TempClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
