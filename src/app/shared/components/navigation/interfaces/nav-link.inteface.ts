export interface NavLinkI {
  url: string;
  title: string;
}
